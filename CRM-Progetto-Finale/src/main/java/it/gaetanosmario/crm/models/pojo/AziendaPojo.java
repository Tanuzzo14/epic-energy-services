package it.gaetanosmario.crm.models.pojo;

import java.math.BigDecimal;
import java.util.Date;

import it.gaetanosmario.crm.models.Contatto;
import it.gaetanosmario.crm.models.Indirizzo;
import it.gaetanosmario.crm.models.TipoAzienda;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AziendaPojo {
	private String nome;
	private String ragioneSociale;
	private long partitaIva;
	private String email;
	private Date dataUltimoContatto;
	private String pec;
	private String telefono;
	private TipoAzienda tipoAzienda;
}
