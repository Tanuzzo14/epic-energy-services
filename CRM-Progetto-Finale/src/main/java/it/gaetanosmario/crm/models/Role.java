package it.gaetanosmario.crm.models;


import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Role extends BaseEntity{
    
   
    @Enumerated(EnumType.STRING)
    private RoleType roleType;
    @Override
    public String toString() {
        return String.format("Role: id%d, roletype%s ", roleType);
        }
}

