package it.gaetanosmario.crm.models;


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
public class Azienda extends BaseEntity{
	private String nome;
	private String ragioneSociale;
	private long partitaIva;
	private String email;
	private Date dataUltimoContatto;
	private String pec;
	private String telefono;
	private BigDecimal fatturatoAnnuale;
	@ManyToOne
	private Contatto contatto;
	@ManyToOne(cascade = CascadeType.ALL)
	private TipoAzienda tipoAzienda;
	@OneToOne
	private Indirizzo indirizzoLegale;
	@OneToOne
	private Indirizzo indirizzoOperativo;
}
