package it.gaetanosmario.crm.models.pojo;

import java.math.BigDecimal;
import java.util.Date;

import it.gaetanosmario.crm.models.TipoAzienda;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfrontoBigDecimalPojo {
	private BigDecimal campo1;
	private BigDecimal campo2;
}
