package it.gaetanosmario.crm.models.pojo;

import java.math.BigDecimal;
import java.util.Date;

import it.gaetanosmario.crm.models.StatoFattura;
import it.gaetanosmario.crm.models.TipoAzienda;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FatturaPojo {
	private Date data;
	private BigDecimal importo;
	private StatoFattura statoFattura;
}
