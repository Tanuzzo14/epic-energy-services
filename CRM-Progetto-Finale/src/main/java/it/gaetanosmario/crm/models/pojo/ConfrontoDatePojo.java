package it.gaetanosmario.crm.models.pojo;

import java.util.Date;

import it.gaetanosmario.crm.models.TipoAzienda;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfrontoDatePojo {
	private Date campo1;
	private Date campo2;
}
