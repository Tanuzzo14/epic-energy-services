package it.gaetanosmario.crm.models.pojo;

import java.util.Date;

import it.gaetanosmario.crm.models.TipoAzienda;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UtentePojo {
	private String username;
	private String nome;
	private String cognome;
	private String password;
}
