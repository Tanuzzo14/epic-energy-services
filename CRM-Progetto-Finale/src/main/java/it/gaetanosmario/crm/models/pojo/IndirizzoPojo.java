package it.gaetanosmario.crm.models.pojo;

import java.util.Date;

import it.gaetanosmario.crm.models.TipoAzienda;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IndirizzoPojo {
	private String via;
	private String civico;
	private String localita;
	private int cap;
}
