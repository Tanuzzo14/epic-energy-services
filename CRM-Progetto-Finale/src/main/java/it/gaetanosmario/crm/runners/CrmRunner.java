package it.gaetanosmario.crm.runners;

import java.io.FileInputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import be06.epicode.cities.CitiesLoader;
import be06.epicode.cities.models.City;
import be06.epicode.cities.models.Province;
import it.gaetanosmario.crm.models.Azienda;
import it.gaetanosmario.crm.models.Citta;
import it.gaetanosmario.crm.models.Contatto;
import it.gaetanosmario.crm.models.Fattura;
import it.gaetanosmario.crm.models.Fattura.FatturaBuilder;
import it.gaetanosmario.crm.models.Indirizzo;
import it.gaetanosmario.crm.models.Provincia;
import it.gaetanosmario.crm.models.Role;
import it.gaetanosmario.crm.models.RoleType;
import it.gaetanosmario.crm.models.StatoFattura;
import it.gaetanosmario.crm.models.TipoAzienda;
import it.gaetanosmario.crm.models.Utente;
import it.gaetanosmario.crm.repositories.AziendaRepository;
import it.gaetanosmario.crm.repositories.CittaRepository;
import it.gaetanosmario.crm.repositories.ContattoRepository;
import it.gaetanosmario.crm.repositories.ProvinciaRepository;
import it.gaetanosmario.crm.repositories.RoleRepository;
import it.gaetanosmario.crm.services.AziendaService;
import it.gaetanosmario.crm.services.ContattoService;
import it.gaetanosmario.crm.services.FatturaService;
import it.gaetanosmario.crm.services.IndirizzoService;
import it.gaetanosmario.crm.services.RoleService;
import it.gaetanosmario.crm.services.StatoFatturaService;
import it.gaetanosmario.crm.services.TipoAziendaService;
import it.gaetanosmario.crm.services.UtenteService;


@Component

public class CrmRunner implements CommandLineRunner{

	@Autowired
	CittaRepository cittaRepository;
	@Autowired
	ProvinciaRepository provinciaRepository;
	@Autowired
	AziendaService aziendaService;
	@Autowired
	ContattoService contattoService;
	@Autowired
	FatturaService fatturaService;
	@Autowired
	IndirizzoService indirizzoService;
	@Autowired
	StatoFatturaService statoFatturaService;
	@Autowired
	TipoAziendaService tipoAziendaService;
	@Autowired
	RoleRepository roleRepository;
	@Autowired
	UtenteService utenteService;
	
	final static Logger log = LoggerFactory.getLogger(CrmRunner.class);
	@Override
	
	
	// nel runner vengono settate in primis le città e in seguito i principali oggetti che servono per avviare correttamente l'applicazione
	public void run(String... args) throws Exception {
		
		//creazione delle città
//		String fileName = "C:\\Users\\Utente\\eclipse-workspace\\CitiesLoader\\src\\Elenco-comuni-italiani.csv";
//		
//		Set<City> cities = CitiesLoader.load(new FileInputStream(fileName));
//
//		for(City city: cities) {
//			Provincia provincia ;
//			Optional<Provincia> p = provinciaRepository.findByAcronym(city.getProvince().getAcronym());
//			if(p.isEmpty()) {
//				provincia = new Provincia(city.getProvince().getName(), city.getProvince().getAcronym());
//				provinciaRepository.save(provincia);
//			}
//			else {
//				provincia = p.get();
//			}
//			Citta c = new Citta(city.getName(), city.isCapital(), provincia);
//			cittaRepository.save(c);
//			
//		}
		//creazione di una fattura e dei dati da cui essa dipende
//		Contatto contattoZuchhetti = new Contatto("mario@zucchetti.com", "mario","rossi", 329999999);
//		contattoService.save(contattoZuchhetti);
////		Indirizzo indirizzoLegaleZucchetti = new Indirizzo("Via zucchetti", "1", "Zona zucchetti", 20122, cittaRepository.findByName("Milano").get(0));
////		indirizzoService.save(indirizzoLegaleZucchetti);
////		Indirizzo indirizzoOperativoZucchetti = new Indirizzo("Via Bitonto", "4", "Zona gnmredd", 76125, cittaRepository.findByName("Trani").get(0));
////		indirizzoService.save(indirizzoOperativoZucchetti);
//		TipoAzienda tipoAzienda = new TipoAzienda("SPA");
//		tipoAziendaService.save(tipoAzienda);
//		Azienda aziendaZucchetti = Azienda.builder()
//				.contatto(contattoZuchhetti)
//				.dataUltimoContatto(new Date(10-02-2022))
//				.nome("Zucchetti")
//				.indirizzoLegale(indirizzoService.getById(1).get())
//				.indirizzoOperativo(indirizzoService.getById(2).get())
//				.fatturatoAnnuale(new BigDecimal(3472899))
//				.ragioneSociale("Consulenza")
//				.email("info@zucchetti.com")
//				.partitaIva(123456789)
//				.pec("zucchetti@pec.it")
//				.telefono("093555555")
//				.build();
//		aziendaService.save(aziendaZucchetti);
//		
//		StatoFattura statoFatturaCompletata = new StatoFattura("Completata");
//		statoFatturaService.save(statoFatturaCompletata);
//		Fattura fatturaZucchetti = Fattura.builder()
//				.azienda(aziendaZucchetti)
//				.data(new Date(14-10-2022))
//				.importo(new BigDecimal(34000))
//				.statoFattura(statoFatturaCompletata)
//				.build();
//		fatturaService.save(fatturaZucchetti);
//		
		//creo l'utente tutti gli oggetti da cui dipende
//		Role admin = new Role(RoleType.ROLE_ADMIN);
//		Role user = new Role(RoleType.ROLE_USER);
//		roleRepository.save(user);
////		roleRepository.save(admin);
//		Set<Role> roles = new HashSet<>();
//		roles.add(roleRepository.findById(1L).get());
//		roles.add(roleRepository.findById(2L).get());
////		
//		Utente utente = new Utente("Gaetano", "Smario", "tanuzzo", "password", roles);
//	utenteService.salvaUtente(utente);
		
		
		
		

	}

}
