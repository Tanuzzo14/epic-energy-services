package it.gaetanosmario.crm.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import it.gaetanosmario.crm.models.Indirizzo;
import it.gaetanosmario.crm.models.pojo.IndirizzoPojo;
import it.gaetanosmario.crm.repositories.CittaService;
import it.gaetanosmario.crm.services.IndirizzoService;

@RestController
@RequestMapping("/indirizzo")
public class IndirizzoController {
	@Autowired
	IndirizzoService indirizzoService;
	@Autowired
	CittaService cittaService;
	
	@PostMapping("/salva/{idCitta}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String salva(@PathVariable int idCitta , @RequestBody  IndirizzoPojo indirizzoPojo ) {
		if(cittaService.getById(idCitta).isEmpty()) {
			return "Inserire prima la città nel DataBase";
		}
		Indirizzo indirizzo = Indirizzo.builder()
				.comune(cittaService.getById(idCitta).get())
				.cap(indirizzoPojo.getCap())
				.civico(indirizzoPojo.getCivico())
				.localita(indirizzoPojo.getLocalita())
				.via(indirizzoPojo.getVia())
				.cap(indirizzoPojo.getCap())
				.build();
		indirizzoService.save(indirizzo);
		return "Indirizzo salvato con successo";
	}
	@PostMapping("/modifica")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String modifica(@PathVariable int idCitta , @RequestBody  IndirizzoPojo indirizzoPojo  ) {
		if(cittaService.getById(idCitta).isEmpty()) {
			return "Inserire prima la città nel DataBase";
		}
		Indirizzo indirizzo = Indirizzo.builder()
				.comune(cittaService.getById(idCitta).get())
				.cap(indirizzoPojo.getCap())
				.civico(indirizzoPojo.getCivico())
				.localita(indirizzoPojo.getLocalita())
				.via(indirizzoPojo.getVia())
				.cap(indirizzoPojo.getCap())
				.build();
		indirizzoService.update(indirizzo);
		return "Indirizzo aggiornato con successo";
	}
	@PostMapping("/elimina/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String elimina(@PathVariable long id) {
		
		return indirizzoService.delete(id);
	}
	
	
}
