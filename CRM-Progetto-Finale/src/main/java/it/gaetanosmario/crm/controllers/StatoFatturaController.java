package it.gaetanosmario.crm.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import it.gaetanosmario.crm.models.StatoFattura;
import it.gaetanosmario.crm.services.StatoFatturaService;

@RestController
@RequestMapping("/statofattura")
public class StatoFatturaController {
	@Autowired
	StatoFatturaService statoFatturaService;
	
	@PostMapping("/salva")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String salva(@RequestBody StatoFattura statoFattura ) {
		statoFatturaService.save(statoFattura);
		return "Fattura salvata con successo";
	}
	@PostMapping("/modifica")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String modifica(@RequestBody  StatoFattura statoFattura) {
		statoFatturaService.update(statoFattura);
		return "Fattura aggiornata con successo";
	}
	@PostMapping("/elimina")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String elimina(@RequestBody StatoFattura statoFattura) {
		
		return statoFatturaService.delete(statoFattura);
	}
}
