package it.gaetanosmario.crm.controllers;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.gaetanosmario.crm.models.Role;
import it.gaetanosmario.crm.models.Utente;
import it.gaetanosmario.crm.models.pojo.UtentePojo;
import it.gaetanosmario.crm.services.RoleService;
import it.gaetanosmario.crm.services.UtenteService;



@RestController
@RequestMapping("/utente")
public class UtenteController {
	@Autowired
	UtenteService utenteService;
	@Autowired
	RoleService roleService;
	@PostMapping("/salvautente/{role}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	
	public String salvaUtente(@RequestBody UtentePojo utentePojo,  @PathVariable boolean role) {
		Set<Role> roles = new HashSet<>();
		roles.add(roleService.getById(1).get());
		if(role == true) {
			roles.add(roleService.getById(2).get());
		}
		Utente utente = Utente.builder()
				.nome(utentePojo.getNome())
				.cognome(utentePojo.getCognome())
				.username(utentePojo.getUsername())
				.password(utentePojo.getPassword())
				.roles(roles)
				.build();
		utenteService.salvaUtente(utente);
		
		return "Utente salvato con successo";
			
	}
}
