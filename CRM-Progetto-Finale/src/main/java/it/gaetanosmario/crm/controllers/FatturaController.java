package it.gaetanosmario.crm.controllers;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.gaetanosmario.crm.models.Azienda;
import it.gaetanosmario.crm.models.Fattura;
import it.gaetanosmario.crm.models.StatoFattura;
import it.gaetanosmario.crm.models.pojo.ConfrontoBigDecimalPojo;
import it.gaetanosmario.crm.models.pojo.ConfrontoDatePojo;
import it.gaetanosmario.crm.models.pojo.FatturaPojo;
import it.gaetanosmario.crm.services.AziendaService;
import it.gaetanosmario.crm.services.FatturaService;
import it.gaetanosmario.crm.services.StatoFatturaService;

@RestController
@RequestMapping("/fattura")
public class FatturaController {
	@Autowired
	AziendaService aziendaService;
	@Autowired
	FatturaService fatturaService;
	@Autowired
	StatoFatturaService statoFatturaService;
	
	// Nei primi cinque  metodi , tutii i request param servono per settare il Pageable
	
	@GetMapping("/perazienda/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Fattura> findByAzienda(@PathVariable long id, @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "2") Integer size,@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort){
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir),sort);
		Azienda a = aziendaService.getById(id).get();
		return fatturaService.getByAzienda(a, paging);
	}
	@PostMapping("/perstatofattura")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Fattura> findByStato(@RequestBody String stato, @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "2") Integer size,@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort){
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir),sort);
		Page<StatoFattura> p = statoFatturaService.findByStato(stato,paging);
		if(p.isEmpty()) {
			return null;
		}
		List<StatoFattura> s = p.getContent();
		return fatturaService.getByStatoFattura(s.get(0), paging);
	}
	@PostMapping("/perdata")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Fattura> findByData(@RequestBody ConfrontoDatePojo confrontoDatePojo, @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "2") Integer size,@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort){
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir),sort);
		return fatturaService.getByData(confrontoDatePojo.getCampo1(), confrontoDatePojo.getCampo2(), paging);
	}
	@PostMapping("/peranno")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Fattura> findByAnno(@RequestBody int anno, @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "2") Integer size,@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort){
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir),sort);
		return fatturaService.getByDataYear(anno, paging);
	}
	@PostMapping("/perimporto")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Fattura> findByImporto(@RequestBody ConfrontoBigDecimalPojo confrontoBigDecimalPojo, @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "2") Integer size,@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort){
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir),sort);
		return fatturaService.getByImportoBetween(confrontoBigDecimalPojo.getCampo1(), confrontoBigDecimalPojo.getCampo2(), paging);
	}
	@PostMapping("/salva/{idAzienda}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String salva(@PathVariable long idAzienda, @RequestBody FatturaPojo fatturaPojo ) {
		if(aziendaService.getById(idAzienda).isEmpty()) {
			return "Inserire prima l'azienda nel database";
		}
		Fattura fattura = Fattura.builder()
				.azienda(aziendaService.getById(idAzienda).get())
				.data(fatturaPojo.getData())
				.importo(fatturaPojo.getImporto())
				.statoFattura(fatturaPojo.getStatoFattura())
				.build();
		fatturaService.save(fattura);
		return "Fattura salvata con successo";
	}
	@PostMapping("/modifica/{idAzienda}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String modifica(@PathVariable long idAzienda, @RequestBody FatturaPojo fatturaPojo ) {
		if(aziendaService.getById(idAzienda).isEmpty()) {
			return "Inserire prima l'azienda nel database  ";
		}
		Fattura fattura = Fattura.builder()
				.azienda(aziendaService.getById(idAzienda).get())
				.data(fatturaPojo.getData())
				.importo(fatturaPojo.getImporto())
				.statoFattura(fatturaPojo.getStatoFattura())
				.build();
		fatturaService.update(fattura);
		return "Fattura aggiornata con successo";
	}
	@PostMapping("/elimina/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String elimina(@PathVariable long id) {
		
		return fatturaService.delete(id);
	}
	
}
