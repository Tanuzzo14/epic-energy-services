package it.gaetanosmario.crm.controllers;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.catalina.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import it.gaetanosmario.crm.models.Azienda;
import it.gaetanosmario.crm.models.TipoAzienda;
import it.gaetanosmario.crm.models.pojo.AziendaPojo;
import it.gaetanosmario.crm.models.pojo.ConfrontoBigDecimalPojo;
import it.gaetanosmario.crm.models.pojo.ConfrontoDatePojo;
import it.gaetanosmario.crm.services.AziendaService;
import it.gaetanosmario.crm.services.ContattoService;
import it.gaetanosmario.crm.services.IndirizzoService;

@RestController
@RequestMapping("/azienda")
public class AziendaController {
	@Autowired
	AziendaService aziendaService;
	@Autowired
	IndirizzoService indirizzoService;
	@Autowired
	ContattoService contattoService;
	
	// Nei primi dieci metodi , tutii i request param servono per settare il Pageable
	
	@GetMapping("/totaleaziendepernome")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Azienda> getAllAllByOrderByNome(@RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "2") Integer size,@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort){
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir),sort);
		return aziendaService.getAllAllByOrderByNome(paging);
		
	}
	@GetMapping("/totaleaziendeperfatturato")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Azienda> getAllByOrderByFatturatoAnnuale(@RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "2") Integer size,@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort){
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir),sort);
		return aziendaService.getAllByOrderByFatturatoAnnuale(paging);
	}
	@GetMapping("/totaleaziendepercreazione")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Azienda> getAllByOrderByCreatedAt(@RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "2") Integer size,@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort){
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir),sort);
		return aziendaService.getAllByOrderByCreatedAt(paging);
	}
	@GetMapping("/totaleaziendeperdataultimocontatto")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Azienda> getAllByOrderByDataUltimoContatto(@RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "2") Integer size,@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort){
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir),sort);
		return aziendaService.getAllByOrderByDataUltimoContatto(paging);
	}
	@GetMapping("/totaleaziendeperacronimo")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Azienda> getAllByOrderByAcronym(@RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "2") Integer size,@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort){
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir),sort);
		return aziendaService.getAllByOrderByAcronym(paging);
	}
	@PostMapping("/aziendeperfatturato/{anno}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Azienda>  getAziendaByFatturatoAnnuale(@PathVariable int anno, @RequestBody ConfrontoBigDecimalPojo confrontoBigDecimalPojo ,@RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "2") Integer size,@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort){
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir),sort);
		return aziendaService.getAziendaByFatturatoAnnuale(anno ,confrontoBigDecimalPojo.getCampo1(), confrontoBigDecimalPojo.getCampo2(), paging);
	}
	@PostMapping("/aziendepercreazione")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Azienda> getAziendaByCreatedAt(@RequestBody ConfrontoDatePojo confrontoDatePojo ,@RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "2") Integer size,@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort){
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir),sort);
		return aziendaService.getAziendaByCreatedAt(confrontoDatePojo.getCampo1(), confrontoDatePojo.getCampo2(), paging);
	}
	@PostMapping("/aziendeperultimocontatto")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Azienda> getAziendaByDataUltimoContatto(@RequestBody ConfrontoDatePojo confrontoDatePojo,@RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "2") Integer size,@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort){
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir),sort);
		return aziendaService.getAziendaByDataUltimoContatto(confrontoDatePojo.getCampo1(), confrontoDatePojo.getCampo2(), paging);
	}
	@PostMapping("/aziendeperpartedinome")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Azienda> getAziendaByNomeContains(@RequestBody String nome,@RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "2") Integer size,@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort){
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir),sort);
		return aziendaService.getAziendaByNomeContains(nome, paging);
	}
	@PostMapping("/salva/{idContatto}/{idIndirizzoLegale}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String salva(@PathVariable int idContatto , @PathVariable int idIndirizzoLegale, @RequestBody AziendaPojo aziendaPojo) {
		if(contattoService.getById(idContatto).isEmpty()) {
			return "Salvare prima il contatto nel DataBase";
		}
		if(indirizzoService.getById(idIndirizzoLegale).isEmpty()) {
			return "Salvare prima l'indirizzo nel DataBase";
		}
		Azienda azienda = Azienda.builder()
				.contatto(contattoService.getById(idContatto).get())
				.dataUltimoContatto(aziendaPojo.getDataUltimoContatto())
				.nome(aziendaPojo.getNome())
				.indirizzoLegale(indirizzoService.getById(idIndirizzoLegale).get())
				.ragioneSociale(aziendaPojo.getRagioneSociale())
				.email(aziendaPojo.getEmail())
				.partitaIva(aziendaPojo.getPartitaIva())
				.pec(aziendaPojo.getPec())
				.telefono(aziendaPojo.getTelefono())
				.tipoAzienda(aziendaPojo.getTipoAzienda())
				.build();
		aziendaService.save(azienda);
		return "Azienda salvata con successo";
	}
	@PostMapping("/modifica")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String modifica(@PathVariable int idContatto , @PathVariable int idIndirizzoLegale,@RequestBody AziendaPojo aziendaPojo) {
		if(contattoService.getById(idContatto).isEmpty()) {
			return "Salvare prima il contatto nel DataBase";
		}
		if(indirizzoService.getById(idIndirizzoLegale).isEmpty()) {
			return "Salvare prima l'indirizzo nel DataBase";
		}
		Azienda azienda = Azienda.builder()
				.contatto(contattoService.getById(idContatto).get())
				.dataUltimoContatto(aziendaPojo.getDataUltimoContatto())
				.nome(aziendaPojo.getNome())
				.indirizzoLegale(indirizzoService.getById(idIndirizzoLegale).get())
				.ragioneSociale(aziendaPojo.getRagioneSociale())
				.email(aziendaPojo.getEmail())
				.partitaIva(aziendaPojo.getPartitaIva())
				.pec(aziendaPojo.getPec())
				.telefono(aziendaPojo.getTelefono())
				.tipoAzienda(aziendaPojo.getTipoAzienda())
				.build();
		aziendaService.update(azienda);
		return "Azienda aggiornata con successo";
	}
	@PostMapping("/elimina/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String elimina(@PathVariable long id) {
		
		return aziendaService.delete(id);
	}
	
	
	
	
	
	
	
	
	
}
