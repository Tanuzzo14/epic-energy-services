package it.gaetanosmario.crm.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import it.gaetanosmario.crm.models.TipoAzienda;
import it.gaetanosmario.crm.services.TipoAziendaService;

@RestController
@RequestMapping("/tipoazienda")
public class TipoAziendaController {
	@Autowired
	TipoAziendaService tipoAziendaService;
	
	@PostMapping("/salva")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String salva(@RequestBody TipoAzienda tipoAzienda ) {
		 tipoAziendaService.save( tipoAzienda);
		return "Fattura salvata con successo";
	}
	
	@PostMapping("/elimina")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String elimina(@RequestBody TipoAzienda tipoAzienda) {
		
		return  tipoAziendaService.delete( tipoAzienda);
	}
}
