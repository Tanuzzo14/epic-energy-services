package it.gaetanosmario.crm.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.gaetanosmario.crm.models.Contatto;
import it.gaetanosmario.crm.services.ContattoService;

@RestController
@RequestMapping("/contatto")
public class ContattoController {
	@Autowired
	ContattoService contattoService;
	
	@PostMapping("/salva")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String salva(@RequestBody Contatto contatto) {
		contattoService.save(contatto);
		return "Contatto salvata con successo";
	}
	@PostMapping("/modifica")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String modifica(@RequestBody  Contatto contatto) {
		contattoService.update(contatto);
		return "Contatto aggiornata con successo";
	}
	@PostMapping("/elimina/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String elimina(@PathVariable long id) {
		
		return contattoService.delete(id);
	}
	
}
