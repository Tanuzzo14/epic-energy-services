package it.gaetanosmario.crm.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.gaetanosmario.crm.models.Contatto;

public interface ContattoRepository extends JpaRepository<Contatto, Long>{

}
