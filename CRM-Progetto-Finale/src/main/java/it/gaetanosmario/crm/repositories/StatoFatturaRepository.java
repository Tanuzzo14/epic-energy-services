package it.gaetanosmario.crm.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import it.gaetanosmario.crm.models.StatoFattura;

public interface StatoFatturaRepository extends JpaRepository<StatoFattura, Long>{
	public Page<StatoFattura> findByStato(String stato,Pageable pageable);
}
