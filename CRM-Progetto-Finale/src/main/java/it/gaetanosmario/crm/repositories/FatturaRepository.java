package it.gaetanosmario.crm.repositories;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.gaetanosmario.crm.models.Azienda;
import it.gaetanosmario.crm.models.Fattura;
import it.gaetanosmario.crm.models.StatoFattura;


public interface FatturaRepository extends JpaRepository<Fattura, Long>{
	//find by
	public Page<Fattura> findByAzienda(Azienda azienda ,Pageable pageable);
	public Page<Fattura> findByStatoFattura(StatoFattura statoFattura ,Pageable pageable);
	public Page<Fattura> findByDataBetween(Date data,Date data2 ,Pageable pageable);
	@Query("select f from Fattura as f where YEAR(f.data) = :anno")
	public Page<Fattura> findByDataYear(int anno ,Pageable pageable);
	public Page<Fattura> findByImportoBetween(BigDecimal primoImporto, BigDecimal secondoImporto,Pageable pageable);
	@Query("select SUM(f.importo) from Fattura as f where YEAR(f.data) = :anno and f.azienda.id = :id")
	public BigDecimal getFatturatoAnnualeByAzienda(int anno, long id);
	
	 
	
}
