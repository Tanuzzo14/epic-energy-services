package it.gaetanosmario.crm.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.gaetanosmario.crm.models.Utente;

public interface UtenteRepository extends JpaRepository<Utente, Long>{

	public Optional<Utente> findByUsername(String username);

}
