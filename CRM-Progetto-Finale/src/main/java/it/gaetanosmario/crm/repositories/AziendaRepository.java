package it.gaetanosmario.crm.repositories;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.gaetanosmario.crm.models.Azienda;

public interface AziendaRepository extends JpaRepository<Azienda, Long>{
	
	// get all
	
	public Page<Azienda> findAllByOrderByNome(Pageable pageable);
	public Page<Azienda> findAllByOrderByFatturatoAnnuale(Pageable pageable);
	public Page<Azienda> findAllByOrderByCreatedAt(Pageable pageable);
	public Page<Azienda> findAllByOrderByDataUltimoContatto(Pageable pageable);
	@Query("select a from Azienda as a order by indirizzoLegale.comune.province.acronym")
	public Page<Azienda> findAllByOrderByAcronym(Pageable pageable);
	
	// get by
	public Page<Azienda> findByFatturatoAnnualeBetween(int anno ,BigDecimal fatturatoAnnuale1,BigDecimal fatturatoAnnuale2 , Pageable pageable);
	public Page<Azienda> findByCreatedAtBetween(Date createdAt1,Date createdAt2 ,Pageable pageable);
	public Page<Azienda> findByDataUltimoContattoBetween(Date dataUltimoContatto1,Date ultimoContatto2 ,Pageable pageable);
	public Page<Azienda> findByNomeContains(String nome,Pageable pageable);
}
