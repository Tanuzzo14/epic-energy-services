package it.gaetanosmario.crm.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.gaetanosmario.crm.models.TipoAzienda;

public interface TipoAziendaRepository extends JpaRepository<TipoAzienda, Long>{

}
