package it.gaetanosmario.crm.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.gaetanosmario.crm.models.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{

}
