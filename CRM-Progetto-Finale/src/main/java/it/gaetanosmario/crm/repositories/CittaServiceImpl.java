package it.gaetanosmario.crm.repositories;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.gaetanosmario.crm.models.Citta;
@Service
public class CittaServiceImpl implements CittaService{
	@Autowired
	CittaRepository cittaRepository;

	@Override
	public Optional<Citta> getById(long id) {
		
		return cittaRepository.findById(id);
	}
}
