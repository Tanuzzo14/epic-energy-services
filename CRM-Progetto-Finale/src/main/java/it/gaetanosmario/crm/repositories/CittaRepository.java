package it.gaetanosmario.crm.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import it.gaetanosmario.crm.models.Citta;

public interface CittaRepository extends JpaRepository<Citta, Long>{
	Page<Citta> findAllByProvinceAcronym(String acronym, Pageable pageable);
	List<Citta> findByName(String name);
}
