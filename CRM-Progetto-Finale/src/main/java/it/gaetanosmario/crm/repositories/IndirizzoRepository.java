package it.gaetanosmario.crm.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.gaetanosmario.crm.models.Indirizzo;

public interface IndirizzoRepository extends JpaRepository<Indirizzo, Long>{

}
