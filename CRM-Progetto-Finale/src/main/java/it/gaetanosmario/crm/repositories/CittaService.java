package it.gaetanosmario.crm.repositories;

import java.util.Optional;

import it.gaetanosmario.crm.models.Citta;

public interface CittaService {
	public Optional<Citta> getById(long id);
}
