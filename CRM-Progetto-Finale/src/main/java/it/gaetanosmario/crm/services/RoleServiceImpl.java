package it.gaetanosmario.crm.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.gaetanosmario.crm.models.Role;
import it.gaetanosmario.crm.repositories.RoleRepository;

@Service
public class RoleServiceImpl implements RoleService{

	@Autowired
	RoleRepository roleRepository;
	
	@Override
	public Optional<Role> getById(long id) {
		return roleRepository.findById(id);
	}

}
