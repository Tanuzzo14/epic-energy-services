package it.gaetanosmario.crm.services;

import java.util.Optional;

import it.gaetanosmario.crm.models.Role;

public interface RoleService {
	public Optional<Role> getById(long id);
}
