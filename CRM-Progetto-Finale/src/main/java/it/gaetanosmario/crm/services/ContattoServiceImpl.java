package it.gaetanosmario.crm.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.gaetanosmario.crm.models.Contatto;
import it.gaetanosmario.crm.repositories.ContattoRepository;

@Service
public class ContattoServiceImpl implements ContattoService{
	@Autowired
	ContattoRepository contattoRepository;

	@Override
	public Contatto save(Contatto contatto) {
		try {
			return contattoRepository.save(contatto);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Contatto update(Contatto contattoAggiornato) {
		try {
			Contatto c = contattoRepository.findById(contattoAggiornato.getId()).get();
			return save(contattoAggiornato);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String delete(long id) {
		try {
			contattoRepository.delete(contattoRepository.findById(id).get());
			return "Contatto eliminato con successo";
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Optional<Contatto> getById(long id) {
		
		return contattoRepository.findById(id);
	}

}
