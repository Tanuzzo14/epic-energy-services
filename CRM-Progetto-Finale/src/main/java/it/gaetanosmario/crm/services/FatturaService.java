package it.gaetanosmario.crm.services;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import it.gaetanosmario.crm.models.Azienda;
import it.gaetanosmario.crm.models.Fattura;
import it.gaetanosmario.crm.models.StatoFattura;

public interface FatturaService {
	//in output tutte le fatture che rientrano in quella azienda
	public Page<Fattura> getByAzienda(Azienda azienda ,Pageable pageable);
	//in output tutte le fatture che rientrano in quello stato 
	public Page<Fattura> getByStatoFattura(StatoFattura statoFattura ,Pageable pageable);
	//in output tutte le fatture che rientrano in quel range di date
	public Page<Fattura> getByData(Date data,Date data2,Pageable pageable);
	//in output tutte le fatture che rientrano in quella azienda
	public Page<Fattura> getByDataYear(int anno ,Pageable pageable);
	//in output tutte le fatture che rientrano in quel range di importi
	public Page<Fattura> getByImportoBetween(BigDecimal primoImporto, BigDecimal secondoImporto,Pageable pageable);
	
	public Optional<Fattura> getById(long id);
	
	public Fattura save(Fattura fattura);
	public Fattura update(Fattura fatturaAggiornata);
	public String delete(long id);
}
