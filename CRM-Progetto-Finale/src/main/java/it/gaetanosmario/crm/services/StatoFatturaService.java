package it.gaetanosmario.crm.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.gaetanosmario.crm.models.StatoFattura;

public interface StatoFatturaService {
	
	public Optional<StatoFattura> getById(long id);
	
	public StatoFattura save(StatoFattura statoFattura);
	public String delete(StatoFattura statoFattura);

	public StatoFattura update(StatoFattura statoFattura);

	public Page<StatoFattura> findByStato(String stato, Pageable pageable);

	
	
	
}
