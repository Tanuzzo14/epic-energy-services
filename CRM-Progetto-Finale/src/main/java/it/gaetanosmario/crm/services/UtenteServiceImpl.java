package it.gaetanosmario.crm.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import it.gaetanosmario.crm.models.Utente;
import it.gaetanosmario.crm.repositories.UtenteRepository;



@Service
public class UtenteServiceImpl implements UtenteService{
	
	@Autowired
	UtenteRepository utenteRepository;
	@Autowired
	PasswordEncoder bCryptPasswordEncoder;

	@Override
	public Utente salvaUtente(Utente utente) {
		try {
			
			String password = bCryptPasswordEncoder.encode(utente.getPassword());
			utente.setPassword(password);
			return utenteRepository.save(utente);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Optional<Utente> cercaPerId(long id) {
		try {
			
			return utenteRepository.findById(id);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
