package it.gaetanosmario.crm.services;

import java.util.Optional;

import it.gaetanosmario.crm.models.Contatto;

public interface ContattoService {
	
	public Contatto save(Contatto contatto);
	public Contatto update(Contatto contattoAggiornato);
	public String delete(long id);
	
	public Optional<Contatto> getById(long id);
	
}
