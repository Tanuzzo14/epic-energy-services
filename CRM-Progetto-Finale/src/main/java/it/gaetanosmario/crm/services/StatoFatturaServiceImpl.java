package it.gaetanosmario.crm.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.gaetanosmario.crm.models.StatoFattura;
import it.gaetanosmario.crm.repositories.StatoFatturaRepository;

@Service
public class StatoFatturaServiceImpl implements StatoFatturaService{

	@Autowired
	StatoFatturaRepository statoFatturaRepository;
	@Override
	public Optional<StatoFattura> getById(long id) {
		
		return statoFatturaRepository.findById(id);
	}

	@Override
	public StatoFattura save(StatoFattura statoFattura) {
		try {
			return statoFatturaRepository.save(statoFattura);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String delete(StatoFattura statoFattura) {
		try {
			statoFatturaRepository.delete(statoFattura);
			return "Stato Fattura eliminato con successo";
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
    public StatoFattura update(StatoFattura statoFattura) {
    try {
        StatoFattura a = statoFatturaRepository.findById(statoFattura.getId()).get();
        if(statoFattura.getId()!=0L) {
            a.setStato(statoFattura.getStato());
            
        }
        return statoFatturaRepository.save(a);
    } catch (Exception e) {
        throw new RuntimeException(e);
    }
        
    }

	@Override
	public Page<StatoFattura> findByStato(String stato,Pageable pageable) {
		// TODO Auto-generated method stub
		return statoFatturaRepository.findByStato(stato,pageable);
	}

	

}
