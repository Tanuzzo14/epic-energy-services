package it.gaetanosmario.crm.services;

import java.util.Optional;

import it.gaetanosmario.crm.models.TipoAzienda;

public interface TipoAziendaService {
	
	public TipoAzienda save(TipoAzienda tipoAzienda);
	public String delete(TipoAzienda tipoAzienda);
	
	public Optional<TipoAzienda> getById(long id);
}
