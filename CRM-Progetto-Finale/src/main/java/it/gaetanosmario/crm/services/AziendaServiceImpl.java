package it.gaetanosmario.crm.services;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.gaetanosmario.crm.models.Azienda;
import it.gaetanosmario.crm.repositories.AziendaRepository;
import it.gaetanosmario.crm.repositories.FatturaRepository;


@Service
public class AziendaServiceImpl implements AziendaService{

	@Autowired
	AziendaRepository aziendaRepository;
	@Autowired
	FatturaRepository fatturaRepository;
	@Override
	public Page<Azienda> getAllAllByOrderByNome(Pageable pageable) {
		try {
			return aziendaRepository.findAllByOrderByNome(pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Azienda> getAllByOrderByFatturatoAnnuale(Pageable pageable) {
		try {	
				return aziendaRepository.findAllByOrderByFatturatoAnnuale(pageable);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
	}

	@Override
	public Page<Azienda> getAllByOrderByCreatedAt(Pageable pageable) {
		try {
			return aziendaRepository.findAllByOrderByCreatedAt(pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Azienda> getAllByOrderByDataUltimoContatto(Pageable pageable) {
		try {
			return aziendaRepository.findAllByOrderByDataUltimoContatto(pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Azienda> getAllByOrderByAcronym(Pageable pageable) {
		try {
			return aziendaRepository.findAllByOrderByAcronym(pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Azienda> getAziendaByFatturatoAnnuale(int anno ,BigDecimal fatturatoAnnuale,BigDecimal fatturatoAnnuale2, Pageable pageable) {
		try {
			return aziendaRepository.findByFatturatoAnnualeBetween( anno ,fatturatoAnnuale, fatturatoAnnuale2, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Azienda> getAziendaByCreatedAt(Date createdAt,Date createdAt2, Pageable pageable) {
		try {
			return aziendaRepository.findByCreatedAtBetween(createdAt, createdAt2, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Azienda> getAziendaByDataUltimoContatto(Date dataUltimoContatto,Date dataUltimoContatto2, Pageable pageable) {
		try {
			return aziendaRepository.findByDataUltimoContattoBetween(dataUltimoContatto, dataUltimoContatto2, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Azienda> getAziendaByNomeContains(String nome, Pageable pageable) {
		try {
			return aziendaRepository.findByNomeContains(nome, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Optional<Azienda> getById(long id) {
		// TODO Auto-generated method stub
		return aziendaRepository.findById(id);
	}

	@Override
	public Azienda save(Azienda azienda) {
		try {
			return aziendaRepository.save(azienda);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Azienda update(Azienda aziendaAggiornata) {
		try {
			Azienda a = aziendaRepository.findById(aziendaAggiornata.getId()).get();
			return aziendaRepository.save(aziendaAggiornata);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String delete(long id) {
		try {

			aziendaRepository.delete(aziendaRepository.findById(id).get());
			return "Azienda eliminata con successo";
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
