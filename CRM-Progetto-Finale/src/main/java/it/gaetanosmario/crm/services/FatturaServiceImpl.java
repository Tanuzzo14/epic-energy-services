package it.gaetanosmario.crm.services;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.gaetanosmario.crm.models.Azienda;
import it.gaetanosmario.crm.models.Fattura;
import it.gaetanosmario.crm.models.StatoFattura;
import it.gaetanosmario.crm.repositories.FatturaRepository;

@Service
public class FatturaServiceImpl implements FatturaService {
	@Autowired
	FatturaRepository fatturaRepository;

	@Override
	public Page<Fattura> getByAzienda(Azienda azienda, Pageable pageable) {
		try {
			return fatturaRepository.findByAzienda(azienda, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Fattura> getByStatoFattura(StatoFattura statoFattura, Pageable pageable) {
		try {
			return fatturaRepository.findByStatoFattura(statoFattura, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Fattura> getByData(Date data,Date data2, Pageable pageable) {
		try {
			return fatturaRepository.findByDataBetween(data,data2, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Fattura> getByDataYear(int anno, Pageable pageable) {
		try {
			return fatturaRepository.findByDataYear(anno, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Fattura> getByImportoBetween(BigDecimal primoImporto, BigDecimal secondoImporto, Pageable pageable) {
		try {
			return fatturaRepository.findByImportoBetween(primoImporto, secondoImporto, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Optional<Fattura> getById(long id) {
		try {
			return fatturaRepository.findById(id);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	@Transactional
	public Fattura save(Fattura fattura) {
		try {
			//creo la data con il giorno di oggi
			long miliseconds = System.currentTimeMillis();
			Date oggi = new Date(miliseconds);
			//creo il Calendar per ricavare l'anno della data in cui è stata emessa la fattura
			Calendar cal = Calendar.getInstance();
			cal.setTime(fattura.getData());
			//creo il Calendar per ricavare l'anno attuale
			Calendar calOggi = Calendar.getInstance();
			cal.setTime(oggi);
			//creo un Big decimal che ha valore 0 , che mi servirà per controllare se ci sono fatture emesse durante l'anno corrente
			BigDecimal zero = new BigDecimal(0);
			//controllo se ci sono fatture emesse durante l'anno corrente
			if(fatturaRepository.getFatturatoAnnualeByAzienda(calOggi.get(Calendar.YEAR), 0).equals(zero) ||fatturaRepository.getFatturatoAnnualeByAzienda(calOggi.get(Calendar.YEAR), 0) == null) {
				// se non ci sono fatture setto il fatturato annuale a 0
				fattura.getAzienda().setFatturatoAnnuale(new BigDecimal(0));
			}
			//se la fattura caricata è dell' anno corrente la sommo al fatturato annuale
			if(calOggi.get(Calendar.YEAR) == cal.get(Calendar.YEAR)) {
				fattura.getAzienda().getFatturatoAnnuale().add(fattura.getImporto());
			}
			//salvo la fattura
			return fatturaRepository.save(fattura);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Fattura update(Fattura fatturaAggiornata) {
		try {
			Fattura f = fatturaRepository.findById(fatturaAggiornata.getId()).get();
			return fatturaRepository.save(fatturaAggiornata);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String delete(long id) {
		try {
			fatturaRepository.delete(fatturaRepository.getById(id));
			return "Fattura eliminata con successo";
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
