package it.gaetanosmario.crm.services;

import java.util.Optional;

import it.gaetanosmario.crm.models.Utente;



public interface UtenteService {
	public Utente salvaUtente(Utente utente);
	
	public Optional<Utente> cercaPerId(long id);
}
