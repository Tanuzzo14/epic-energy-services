package it.gaetanosmario.crm.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.gaetanosmario.crm.models.TipoAzienda;
import it.gaetanosmario.crm.repositories.TipoAziendaRepository;

@Service
public class TipoAziendaServiceImpl implements TipoAziendaService {

	
	@Autowired
	TipoAziendaRepository tipoAziendaRepository;
	
	@Override
	public TipoAzienda save(TipoAzienda tipoAzienda) {
		try {
			return tipoAziendaRepository.save(tipoAzienda);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String delete(TipoAzienda tipoAzienda) {
		try {
			tipoAziendaRepository.delete(tipoAzienda);
			return "Tipo Azienda eliminato con successo";
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Optional<TipoAzienda> getById(long id) {
		return tipoAziendaRepository.findById(id);
	}

}
