package it.gaetanosmario.crm.services;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import it.gaetanosmario.crm.models.Azienda;


public interface AziendaService {
	//in output tutte le aziende ordinate per nome
	public Page<Azienda> getAllAllByOrderByNome(Pageable pageable);
	//in output tutte le aziende ordinate per fatturato annuale
	public Page<Azienda> getAllByOrderByFatturatoAnnuale(Pageable pageable);
	//in output tutte le aziende ordinate per data di inserimento al database
	public Page<Azienda> getAllByOrderByCreatedAt(Pageable pageable);
	//in output tutte le aziende ordinate per data dell' ultimo contatto con l'azienda
	public Page<Azienda> getAllByOrderByDataUltimoContatto(Pageable pageable);
	//in output tutte le aziende ordinate per l'acronimo della provincia di appartenenza
	public Page<Azienda> getAllByOrderByAcronym(Pageable pageable);
	//in output le aziende che rientrano nel range dei due fatturati
	public Page<Azienda> getAziendaByFatturatoAnnuale(int anno ,BigDecimal fatturatoAnnuale1,BigDecimal fatturatoAnnuale2 , Pageable pageable);
	//in output le aziende che rientrano nel range delle due date di creazione
	public Page<Azienda> getAziendaByCreatedAt(Date createdAt,Date createdAt2 ,Pageable pageable);
	//in output le aziende che rientrano nel range delle due date di ultimo contatto con le aziende
	public Page<Azienda> getAziendaByDataUltimoContatto(Date dataUltimoContatto,Date dataUltimoContatto2,Pageable pageable);
	//in output le aziende che rientrano hanno la stringa inserita nel nome
	public Page<Azienda> getAziendaByNomeContains(String nome,Pageable pageable);
	
	public Optional<Azienda> getById(long id);
	
	public Azienda save(Azienda azienda);
	public Azienda update(Azienda aziendaAggiornata);
	public String delete(long id);
}
