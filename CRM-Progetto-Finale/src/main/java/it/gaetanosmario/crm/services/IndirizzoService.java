package it.gaetanosmario.crm.services;

import java.util.Optional;

import it.gaetanosmario.crm.models.Indirizzo;

public interface IndirizzoService {
	
	public Optional<Indirizzo> getById(long id);
	
	public Indirizzo save(Indirizzo indirizzo);
	public Indirizzo update(Indirizzo indirizzoAggiornato);
	public String delete(long id);
}
