package it.gaetanosmario.crm.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.gaetanosmario.crm.models.Indirizzo;
import it.gaetanosmario.crm.repositories.IndirizzoRepository;

@Service
public class IndirizzoServiceImpl implements IndirizzoService {

	@Autowired
	IndirizzoRepository indirizzoRepository;
	
	@Override
	public Optional<Indirizzo> getById(long id) {
		// TODO Auto-generated method stub
		return indirizzoRepository.findById(id);
	}

	@Override
	public Indirizzo save(Indirizzo indirizzo) {
		try {
			return indirizzoRepository.save(indirizzo);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Indirizzo update(Indirizzo indirizzoAggiornato) {
		try {
			Indirizzo i = indirizzoRepository.getById(indirizzoAggiornato.getId());
			return indirizzoRepository.save(indirizzoAggiornato);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String delete(long id) {
		try {
			indirizzoRepository.delete(indirizzoRepository.getById(id));
			return "Indirizzo Eliminato con successo";
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
