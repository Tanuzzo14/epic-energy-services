package it.gaetanosmario.crm;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import it.gaetanosmario.crm.models.Azienda;
import it.gaetanosmario.crm.models.pojo.ConfrontoBigDecimalPojo;
import it.gaetanosmario.crm.models.pojo.ConfrontoDatePojo;
import it.gaetanosmario.crm.services.AziendaService;
import it.gaetanosmario.crm.services.FatturaService;


@SpringBootTest
class CrmProgettoFinaleApplicationTests {
	
	@Autowired
	AziendaService aziendaService;
	@Autowired
	FatturaService fatturaService;
	Pageable paging;
	ConfrontoDatePojo confrontoDatePojo;
	ConfrontoBigDecimalPojo confrontoBigDecimalPojo;
	Azienda aziendaTest;
	
	//nel metodo seguente istanzio ciò che mi servirà per far funzionare i test correttamente
	@BeforeEach
	public void setUp() throws Exception {
		
		paging = PageRequest.of(0,1 ,Sort.Direction.ASC,"id");
		confrontoDatePojo = new ConfrontoDatePojo(new Date(5-14-2001),new Date(14-10-2022));
		confrontoBigDecimalPojo = new ConfrontoBigDecimalPojo(new BigDecimal(0), new BigDecimal(1000000000));
		aziendaTest = aziendaService.getById(2).get();
	}
	
	//in ogni test di azienda controllo se la pagina ritornata contiene il numero corretto di elementi
	@Test
	public void testGetAllByNome() {
		assertThat(aziendaService.getAllAllByOrderByNome(paging).getSize()).isEqualTo(1);
	}
	@Test
	public void testGetAllByFatturatoAnnuale() {
		assertThat(aziendaService.getAllByOrderByFatturatoAnnuale(paging).getSize()).isEqualTo(1);
	}
	@Test
	public void testGetAllByAcronym() {
		assertThat(aziendaService.getAllByOrderByAcronym(paging).getSize()).isEqualTo(1);
	}
	@Test
	public void testGetAllByCreatedAt() {
		assertThat(aziendaService.getAllByOrderByCreatedAt(paging).getSize()).isEqualTo(1);
	}
	@Test
	public void testGetByDate() {
		assertThat(aziendaService.getAziendaByCreatedAt(confrontoDatePojo.getCampo1(),confrontoDatePojo.getCampo2(),paging).getSize()).isEqualTo(1);
	}
	@Test
	public void testGetByNomeContains() {
		assertThat(aziendaService.getAziendaByNomeContains("zie",paging).getSize()).isEqualTo(1);
	}
	
	//test di fattura service
	//anche qui testo se la pagina ritornata contiene il numero corretto di elementi
	public void testGetByImportoBeetween() {
		assertThat(fatturaService.getByImportoBetween(confrontoBigDecimalPojo.getCampo1(), confrontoBigDecimalPojo.getCampo2(), paging).getSize()).isEqualTo(1);
	}
	@Test
	public void testGetByAzienda() {
		assertThat(fatturaService.getByAzienda(aziendaTest, paging).getSize()).isEqualTo(1);
	}

}
